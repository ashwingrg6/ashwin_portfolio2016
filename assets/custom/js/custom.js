function animateLightBulb(){
  $light_bulb = $('.welcome-left-bulb');
  $light_bulb.animate({left: '5px'});
  $light_bulb.animate({top: '3px'});
  $light_bulb.animate({top: '0px'});
  $light_bulb.animate({left: '0px'});
  setTimeout(animateLightBulb, 1000);
}
$(document).ready(function () {
  //skill-bar
  jQuery('.skillbar').each(function(){
		jQuery(this).find('.skillbar-bar').animate({
			width:jQuery(this).attr('data-percent')
		},3000);
	});
  animateLightBulb();
  var App = {
    registerGlobalDomFunctions: function(){
      //Sidebar links on mouseover event
      $('a.sidebar-page').on('mouseover', function(e){
        $this = $(this);
        $this
          .find('i.fa').hide()
          .end()
          .find('p').show();
      });
      //Sidebar links on mouseout event
      $('a.sidebar-page').on('mouseout', function(e){
        $this = $(this);
        $this
          .find('p').hide()
          .end()
          .find('i.fa').show();
      });
      //contact button mouseover event
      $('a.home-contact-btn').on('mouseover', function(e){
        $this = $(this);
        $this.animate({padding: '8px 20px'});
        return false;
      });
      //contact button mouseout event
      $('a.home-contact-btn').on('mouseout', function(e){
        $this = $(this);
        $this.animate({padding: '8px 10px'});
        return false;
      });
      Controller.homePageTextAnimate("span.hi-text");
      Controller.homePageTextAnimate("span.name-text");
      Controller.homePageTextAnimate("span.designation-text");
      Controller.pageTitleAnimate("span.page-title");
      //contact page
      $('.contact-form-control').on("focus", function(e){
        $this = $(this);
        $this.attr("placeholder", '')
              .css("outline","none")
              .css("border","none");
      });
      $('.contact-form-control').on("focusout", function(e){
        $this = $(this);
        $this.attr("placeholder", $this.data("holder"));
      });
      //contact page ends
      //sidebar menu for loader
      $('a.sidebar-page').on('click', function(e){
        loader_bar = $('.loader-bar');
        loader_bar
          .show()
          .animate({
            width: '100%'
          }, 1500);
      });
    },
  };

  //Controller
  var Controller = {
    homePageTextAnimate: function(selector){
      var str = $(selector).text();
      var result = "";
      str = str.split("");
      $.each(str,function() {
        if(selector == "span.name-text" && this == "A"){
          result += '<span class="color-sky-blue">' + this + '</span>';
        }else{
          result += '<span class="text-animate">' + this + '</span>';
        }
      });
      $(selector).html($(result));
    },
    pageTitleAnimate: function(selector){
      var str = $(selector).text();
      var result = "";
      str = str.split("");
      $.each(str,function() {
        result += '<span class="page-title-animate">' + this + '</span>';
      });
      $(selector).html($(result));
    }
  };
  App.registerGlobalDomFunctions();
});